import { configureStore } from "@reduxjs/toolkit";

import tictactoeReducer, {
  tictactoeSliceName,
} from "../features/tictactoe/tictactoeSlice";

import hangmanReducer, {
  hangmanSliceName,
} from "../features/hangman/hangmanSlice";

import sudokuReducer, {
  sudokuSliceName,
} from '../features/sudoku/SudokuSlice';

import snakeReducer, {
  snakeSliceName,
} from '../features/snake/SnakeSlice';



export default configureStore({
  reducer: {
    [tictactoeSliceName]: tictactoeReducer,
    [hangmanSliceName]: hangmanReducer,
    [sudokuSliceName]: sudokuReducer,
    [snakeSliceName]: snakeReducer,
  },
});
