import { createSlice } from '@reduxjs/toolkit';
import { makepuzzle, solvepuzzle } from 'sudoku';

export const sudokuSliceName = 'sudoku';

const sudokuSlice = createSlice({
  name: sudokuSliceName,
  initialState: { 
      isWinner:false,
      isStarted: false,
      sudoku: [],
      solution: [],
      showSolution: false,
      showKeyboard: false,
      currentIndex: null,
   },
  reducers: {
    doToggleStart: (state) => {  
    
        if (state.isStarted) {
            state.isWinner = false;
            state.sudoku = [];
            state.solution = [];
            state.showSolution = false;
            state.showKeyboard = false;
        }

        state.isStarted = !state.isStarted;
    },
    doGenerateSudoku: (state) => {  
        const puzzle = makepuzzle();
        const solve = solvepuzzle(puzzle);
        state.sudoku = puzzle;
        state.solution = solve;
    },
    doTogglekeyboard: (state, action) => {
        const { showKeyboard, currentIndex } = action.payload;
        state.showKeyboard = showKeyboard;
        state.currentIndex = currentIndex;
    },
    doSetSudoku: (state, action) => {
        state.sudoku = action.payload;
    },
    doShowSolution: (state, action) => {
        state.showSolution = action.payload;
    },
    doToggleWinner: (state, action) => {
        state.isWinner = action.payload;

    }
   }
});

// reducers
export const { 
    doToggleStart,
    doTogglekeyboard,
    doGenerateSudoku, 
    doToogleKeyboard, 
    doSetSudoku, 
    doShowSolution,
    doToggleWinner
} = sudokuSlice.actions;

// selector
export const selectIsStarted = (state) => state[sudokuSliceName].isStarted;
export const selectSolution = (state) => state[sudokuSliceName].solution;
export const selectShowSolution = (state) => state[sudokuSliceName].showSolution;
export const selectSudoku = (state) => state[sudokuSliceName].sudoku;
export const selectShowKeyboard = (state) => state[sudokuSliceName].showKeyboard;
export const selectCurrentIndex = (state) => state[sudokuSliceName].currentIndex;

export default sudokuSlice.reducer;
