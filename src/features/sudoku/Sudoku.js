import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import {
  doToggleStart,
  doGenerateSudoku,
  selectIsStarted,
  selectSudoku,
  selectSolution,
  selectShowSolution,
  doShowSolution,
  doSetSudoku,
  doTogglekeyboard,
  selectCurrentIndex,
  selectShowKeyboard,
  doToggleWinner,
} from "./SudokuSlice";

import StartButton from "../../components/StartButton";
import Keyboard from "../../components/Keyboard";
import "../../styles/Sudoku.scss";

export default function Sudoku() {
  const dispatch = useDispatch();

  const isStarted = useSelector(selectIsStarted);
  const sudoku = useSelector(selectSudoku);
  const solution = useSelector(selectSolution);
  const showSolution = useSelector(selectShowSolution);
  const currentIndex = useSelector(selectCurrentIndex);

  const board = showSolution ? solution : sudoku;

  useEffect(() => {
    if (isStarted) {
      dispatch(doGenerateSudoku());
    }
  }, [isStarted]);

  useEffect(() => {
    if (solution === sudoku) {
      dispatch(doToggleWinner(true));
      console.log('Has ganado')
    }
  }, [board]);

  return (
    <div className="Sudoku">
      <StartButton
        onClick={() => dispatch(doToggleStart())}
        isStarted={isStarted}
      />
      {isStarted ? (
        <div className="Sudoku__board">
          {board.map((cell, index) => (
            <button
              key={index}
              onClick={() => {
                dispatch(
                  doTogglekeyboard({ showKeyboard: true, currentIndex: index })
                );
              }}
            >
              {cell}
            </button>
          ))}
        </div>
      ) : null}
      {isStarted ? (
        <div className="Sudoku__menu">
        <button className="Sudoku__button" onClick={() => dispatch(doShowSolution(true))}>
            Show solution
          </button>
          <button className="Sudoku__button" 
            onClick={() => {
              dispatch(doGenerateSudoku());
              dispatch(doShowSolution(false));
            }}
          >
            Start Again
          </button>
        </div>
      ) : null}

      {useSelector(selectShowKeyboard) ? (
        <Keyboard
          elements={"0123456789"}
          handleClick={(num) => {
            const newSudoku = sudoku.map((cell, index) =>
              index === currentIndex ? num : cell
            );

            dispatch(doSetSudoku(newSudoku));
            dispatch(
              doTogglekeyboard({ showKeyBoard: false, currentIndex: null })
            );
          }}
        />
      ) : null}
    </div>
  );
}
