import React from "react";
import { useSelector } from 'react-redux'
import "../../styles/Snake.scss";
import { selectSnakeDots} from './SnakeSlice'
export default ()=> {
    const SnakeDot = useSelector(selectSnakeDots);

    return(
  <div className = 'Snake'>
    {SnakeDot.map((dot, index) => {
      const positionDots = {
        left: `${dot[0]}rem`,
        top: `${dot[1]}rem`,
      }
      return <div className="Snake__dot" key={index} style={positionDots}></div>;
    })}
  </div>
    )
}
