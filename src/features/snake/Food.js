import React from 'react'
import "../../styles/Snake.scss";

export default (props) =>{
    const positionFoodDots = {
        left: `${props.positiondot[0]}rem`,
        top: `${props.positiondot[1]}rem`,
      }
    return (
        <div className = 'Snake'>
        <div className = 'Snake__fooddot' style= {positionFoodDots}>
            
        </div></div>
    )
}
