import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import SnakeDots from "./SnakeDots";
import Food from "./Food";
import StartButton from '../../components/StartButton';
import "../../styles/Snake.scss";
import {
    doToggleStart,
  selectIsStarted,
  selectSnakeDots,
  selectfooddots,
  doRandomFoodPosition,
  doDirectionSnake,
  selectdirection,
  doMoveSnake,
  selectspeed,
  docheckIfOutOfBorders,
  docheckIfCollapsed,
  docheckIfEat,
} from "./SnakeSlice";

export default function Snake() {
  const dispatch = useDispatch();
  const isStarted = useSelector(selectIsStarted);
  const direction = useSelector(selectdirection);
  const SnakeDot = useSelector(selectSnakeDots);
  const food = useSelector(selectfooddots);
  const speed = useSelector(selectspeed);

  useEffect(() => {
    dispatch(doRandomFoodPosition());
  }, [isStarted]);
  
  useEffect(() => {
    dispatch(docheckIfOutOfBorders());
    dispatch(docheckIfCollapsed());
    dispatch(docheckIfEat());
    
    const interval = setInterval(() => dispatch(doMoveSnake()), speed);
    return () => {
      clearInterval(interval);
    };
  }, [SnakeDot]);


  useEffect(() => {
    document.onkeydown = onKeyDown;
    dispatch(doMoveSnake());

  }, [direction]);

  const onKeyDown = (e) => {
    e = e || window.event;
    switch (e.keyCode) {
      case 38:
        dispatch(doDirectionSnake("UP"));
        break;
      case 40:
        dispatch(doDirectionSnake("DOWN"));
        break;
      case 37:
        dispatch(doDirectionSnake("LEFT"));
        break;
      case 39:
        dispatch(doDirectionSnake("RIGHT"));
        break;
    }

  };

  return (
<>
    <StartButton
    onClick={() => dispatch(doToggleStart())}
    isStarted={isStarted}
  />
 {isStarted ? (
    <div className="Snake">
      <div className="Snake__square">
        <SnakeDots SnakeDot={SnakeDot}></SnakeDots>
        <Food positiondot={food}></Food>
      </div>
    </div>):null}
    </>
  );
}
