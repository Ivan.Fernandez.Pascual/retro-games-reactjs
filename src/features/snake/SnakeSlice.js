import { createSlice } from "@reduxjs/toolkit";

export const snakeSliceName = "snake";
const snakeSlice = createSlice({
  name: snakeSliceName,
  initialState: {
    isStarted: false,
    fooddots: [],
    snakeDots: [
      [0, 0],
      [1, 0],
      [2, 0],
    ],
    direction: "RIGHT",
    speed: 500,
  },

  reducers: {
    doToggleStart: (state) => {  
    
        if (state.isStarted) {
            state.fooddots= [];
            state.snakeDots= [
              [0, 0],
              [1, 0],
              [2, 0],
              
            ];
            state.direction= "RIGHT";
            state.speed= 500
        }

        state.isStarted = !state.isStarted;
    },
    doRandomFoodPosition: (state) => {
      let min = 1;
      let max = 17;
      let x = Math.floor((Math.random() * (max - min + 1) + min) / 2) * 2;
      let y = Math.floor((Math.random() * (max - min + 1) + min) / 2) * 2;
      state.fooddots = [x, y];
    },
    doDirectionSnake: (state, action) => {
      state.direction = action.payload;
    },
    doMoveSnake: (state) => {
      let dots = [...state.snakeDots];
      let head = dots[dots.length - 1];

      switch (state.direction) {
        case "RIGHT":
          head = [head[0] + 1, head[1]];
          break;
        case "LEFT":
          head = [head[0] - 1, head[1]];
          break;
        case "DOWN":
          head = [head[0], head[1] + 1];
          break;
        case "UP":
          head = [head[0], head[1] - 1];
          break;
      }
      dots.push(head);
      dots.shift();
      state.snakeDots = dots;
    },

    docheckIfOutOfBorders(state) {
      let head = state.snakeDots[state.snakeDots.length - 1];
      if (head[0] >= 18 || head[1] >= 18 || head[0] < 0 || head[1] < 0) {
        alert("YOU CRASH!! YOUR SCORE WAS: " + state.snakeDots.length);

        state.snakeDots = [
          [0, 0],
          [1, 0],
          [2, 0],
        ];
        state.direction = "RIGHT";
        state.speed = 500;
      }
    },
    docheckIfCollapsed(state) {
      let snake = [...state.snakeDots];
      let head = snake[snake.length - 1];
      snake.pop();
      snake.forEach((dot) => {
        if (head[0] === dot[0] && head[1] === dot[1]) {
            alert("YOU CRASH!! YOUR SCORE WAS: " + state.snakeDots.length);

          state.snakeDots = [
            [0, 0],
            [1, 0],
            [2, 0],
          ];
          state.direction = "RIGHT";
          state.speed = 500;
        }
      });
    },
    docheckIfEat(state) {
      let head = state.snakeDots[state.snakeDots.length - 1];
      let food = state.fooddots;
      if (head[0] === food[0] && head[1] === food[1]) {
        let min = 1;
        let max = 17;
        let x = Math.floor((Math.random() * (max - min + 1) + min) / 2) * 2;
        let y = Math.floor((Math.random() * (max - min + 1) + min) / 2) * 2;

        let newSnake = [...state.snakeDots];
        newSnake.unshift([]);
        state.snakeDots = newSnake;

        state.fooddots = [x, y];
        if (state.speed > 10) {
          state.speed = state.speed - 40;
        }
      }
    },
  },
});

// Cada reducer se convierte en una acción y puedo exportarla
export const {
    doToggleStart,
  doRandomFoodPosition,
  doDirectionSnake,
  doMoveSnake,
  docheckIfOutOfBorders,
  docheckIfCollapsed,
  docheckIfEat,
} = snakeSlice.actions;

export const selectIsStarted = (state) => state[snakeSliceName].isStarted;
export const selectSnakeDots = (state) => state[snakeSliceName].snakeDots;
export const selectfooddots = (state) => state[snakeSliceName].fooddots;
export const selectdirection = (state) => state[snakeSliceName].direction;
export const selectspeed = (state) => state[snakeSliceName].speed;

export default snakeSlice.reducer;
