import React from 'react';
import { Link } from 'react-router-dom';

import '../styles/Home.scss';

const routes = [
  {
    name: 'Snake',
    to: '/snake',
    img:
      'https://mundoretorcido.files.wordpress.com/2013/05/gif-best-snake-game.gif?w=299',
  },
  {
    name: 'TicTacToe',
    to: '/tictactoe',
    img: '/assets/tictactoe.gif',
  },
  {
    name: 'Hangman',
    to: '/hangman',
    img: '/assets/hangman.gif',
  },
  {
    name: 'Sudoku',
    to: '/sudoku',
    img:
      'https://natalidossier.files.wordpress.com/2016/04/sudoku1.gif?w=736',
  }
];

export default function Home() {
  return (
    <div className="Home">
      <h1 className="Home__title">
        <span aria-label="game image" role="img">👾</span> Selecciona tu juego{' '}
        <span  aria-label="game image" role="img">👾</span>
      </h1>
      <ul className="Home__list">
        {routes.map(({ name, to, img }) => (
          <li key={to}>
            <Link to={to}>
              <img src={img} alt={name} />
              <h3>{name}</h3>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
}
